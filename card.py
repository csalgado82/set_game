import constants

class Card():
    def __init__(self, id=None, number=None, color=None, shape=None, filled=None):
        self.id = int(id)
        self.number = int(number)
        self.color = int(color)
        self.shape = int(shape)
        self.filled = int(filled)
    
    def __str__(self):
        if self.number == 1:
            aux_string = 'una' if self.shape == 3 else 'un'
            return 'Carta con %s %s %s de color %s' %(
                aux_string,
                constants.shapes[self.shape - 1],
                constants.fillers[self.filled - 1],
                constants.colors[self.color - 1]
            )
        else:
            return 'Carta con %s %ss %ss de color %s' %(
                self.number,
                constants.shapes[self.shape - 1],
                constants.fillers[self.filled - 1],
                constants.colors[self.color - 1]
            )

    def __eq__(self, other):
        return (
            self.number == other.number and
            self.color == other.color and
            self.shape == other.shape and
            self.filled == other.filled
        )

    def __lt__(self, other):
        return self.id < other.id
