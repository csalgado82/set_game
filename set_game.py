import logging

from card import Card

# Function to get cards board of txt
def get_cards_board_from_txt():
    card_string = []
    cards = []
    try:
        with open('cards.txt') as f:
            card_string = f.readlines()

        for i, c in enumerate(card_string):
            aux_card = c.split(',')
            card = Card((i + 1), aux_card[0], aux_card[1], aux_card[2], aux_card[3])
            cards.append(card)
    except Exception as e:
        logging.exception('Ocurrio un error al crear el tablero de cartas')
        exit()

    return cards

# Function to generate all possible couple combinations
def generate_couple_cards(cards):
    pairs_cards = []

    try:
        for c in cards:
            for a in cards:
                if c != a and not [a, c] in pairs_cards:
                    pairs_cards.append([c, a])
    except Exception as e:
        pass
    
    return pairs_cards

# Function to generate a possible card to complete the set
def get_set_of_pair(pair):
    set_card = []

    if pair[0].number == pair[1].number:
        set_card.append(pair[0].number)
    else:
        set_card.append(6 - pair[0].number - pair[1].number) 

    if pair[0].color == pair[1].color:
        set_card.append(pair[0].color)
    else:
        set_card.append(6 - pair[0].color - pair[1].color) 

    if pair[0].shape == pair[1].shape:
        set_card.append(pair[0].shape)
    else:
        set_card.append(6 - pair[0].shape - pair[1].shape) 
    
    if pair[0].filled == pair[1].filled:
        set_card.append(pair[0].filled)
    else:
        set_card.append(6 - pair[0].filled - pair[1].filled) 

    return set_card

# Function to search all sets of the game
def get_sets_cards(cards, pairs_cards):
    sets = []

    for p in pairs_cards:
        for c in cards:
            if not c in p:
                # Get possible set of cards and check is match
                if get_set_of_pair(p) == [c.number, c.color, c.shape, c.filled]:
                    aux_set = p
                    aux_set.append(c)
                    aux_set = sorted(aux_set)
                    if not aux_set in sets:
                        sets.append(aux_set)
                else:
                    continue
    return sets

# Main function
def set_game():
    cards = get_cards_board_from_txt()
    if not cards or len(cards) == 0:
        logging.exception('Tablero de cartas vacio')
        exit()

    pairs_cards = generate_couple_cards(cards)
    if not pairs_cards or len(pairs_cards) == 0:
        logging.exception('Ocurrio al buscar las coincidencias revisa las cartas del tablero')
        exit()

    sets = get_sets_cards(cards, pairs_cards)
    if not sets or len(sets) == 0:
        logging.exception('Ocurrio al buscar las coincidencias revisa las cartas del tablero')
        exit()

    print('----------------------------------------------------------------')
    print('Se encontraron %s sets en el tablero de cartas' % len(sets))
    print('----------------------------------------------------------------')
    for i, c in enumerate(sets):
        print('Set #%s' % (i+1))
        print(c[0])
        print(c[1])
        print(c[2])
        print('----------------------------------------------------------------')

# Execute main function
set_game()