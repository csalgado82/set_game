# TEST SET JUEGO DE CARTAS - BEDU

Script para identificar los sets existentes en el tablero de cartas

## Requisitos

Para ejecutar el script es necesario tener las cartas del tablero en el archivo cards.txt, basando el valor número de cada opción para generar correctamente las cartas:

NÚMEROS:
- UNO = 1
- DOS = 2
- TRES = 3

COLORES:
- ROJO = 1
- VERDE = 2
- MORADO = 3

FORMA:
- DIAMANTE = 1
- OVALO = 2
- ONDA = 3

RELLENOS:
- RELLENO = 1
- RALLADO = 2
- VACIO = 3

Y en el orden siguiente separado por comas:

NÚMERO,COLOR,FORMA,RELLENO

Posterior a validar que el archivo cards.txt tenga todas las cartas del tablero, ejecutamos el siguiente comando y nos mostrará en pantalla en número de sets así como las cartas que conforman cada uno 

```bash
python ./set_game.py
```